package praktikum4;

public class Tabel6 {

	public static void main(String[] args) {

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (i + j < 10) {
					System.out.print(j + i + " ");
				} else {
					System.out.print(j + i - 10 + " ");
				}
			}
			System.out.println();
		}
	}	
}
