package praktikum2;

import java.util.Scanner;

public class GrupiSuurus {
	
	public static void main(String[] args) {
		
		// 7 jagada 3-ga on 2 ja jääk 1
		// 7 % 3 => 1
		

		System.out.println("Sisesta inimeste arv: ");
		Scanner sisestus = new Scanner (System.in);
		int inArv = sisestus.nextInt();
		
		int grupisuurus = 20;
		int grupiarv = inArv / grupisuurus;
		
		
		int jaak = inArv % grupiarv;
				
		System.out.println(inArv + " inimesest saab moodustada " + grupiarv + " gruppi. Üle jääb " + jaak + " inimest.");			
		
		sisestus.close();
	}

}
