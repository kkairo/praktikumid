package praktikum10;

public class Rekursioon {
	
	public static void main(String[] args) {
		
		System.out.println(astenda(2, 3));
		// astenda(2, 3) => 2 * astenda(2, 2) 
		// astenda(2, 2) => 2 * astenda(2, 1)
		// astenda(2, 1) => 2
	}

	public static int astenda(int arv, int aste) {
		// astenda(2, 3) => 2 * astenda(2, 2) 
		// l6petamise tingimus
		if (aste == 1) // if (aste == 0)
			return arv; // return 1;
		else
			return arv * astenda(arv, aste - 1);
	}
	
	}


