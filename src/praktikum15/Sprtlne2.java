package praktikum15;

public class Sprtlne2 {

//Sportlase esinemist hindab n>2 kohtunikku. Hinnete hulgast eemaldatakse kõige madalam ja
//kõige kõrgem ning leitakse ülejäänud n-2 hinde aritmeetiline keskmine. 
//Kirjutada Java-meetod hinde arvutamiseks.


	    public static void main (String[] args) {
	        System.out.println (result (new double[]{0., 1., 2., 3., 4.}));
	        // YOUR TESTS HERE
	    }

	    public static double result (double[] marks) {
	        double minValue = Double.MAX_VALUE;
	        int minIndex = 0;
	        double maxValue = Double.MIN_VALUE;
	        int maxIndex = 0;
	        double resultSum = 0;

	        for(int i = 0; i < marks.length; i++){
	            //min value
	            if(marks[i] < minValue){
	                minValue = marks[i];
	                minIndex = i;
	            }
	            //max value
	            if(marks[i] > maxValue){
	                maxValue = marks[i];
	                maxIndex = i;
	            }
	        }
	        //sum
	        for (int i = 0; i < marks.length; i++){
	            if (i != minIndex && i != maxIndex){
	                resultSum += marks[i];
	            }
	        }

	        return resultSum / (marks.length - 2);
	    }	
	
}
