package praktikum15;

import java.util.Arrays;

public class Mck2 {

	public static void main(String[] args) {
		int d[] = new int[] { 5, 1, 2, 3, 4, 6 };
		int matrix[][] = new int[][] { { 1, 2, 10 }, { 1, 2 }, { 1, 2, 3, 4 } };
		// 1.
		// System.out.println(aKesk(d));

		// 2.
		// int n = 8;
		// for (int i = 0; i < n; i++) {
		// for (int j = 0; j < n; j++) {
		// System.out.print(maatriks(n)[i][j]);
		// }
		// System.out.println();
		// }

		// 3.
		// System.out.println(skoor(d));

		// 4.


			System.out.println(veergudeSummad(matrix));
		

	}
	// Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d pőhjal
	// niisuguste elementide arvu, mis on rangelt suuremad kőigi elementide
	// aritmeetilisest keskmisest (aritmeetiline keskmine = summa /
	// elementide_arv).

	public static int aKesk(int d[]) {

		int summa = 0;
		for (int i = 0; i < d.length; i++) {
			summa = summa + d[i];
		}
		int aKesk = summa / d.length;

		int count = 0;
		for (int i = 0; i < d.length; i++) {
			if (d[i] > aKesk) {
				count = count + 1;
			}
		}
		return count;
	}

	// Koostage Java meetod, mis genereerib parameetrina etteantud n järgi
	// niisuguse n korda n täisarvumaatriksi, mille iga elemendi väärtuseks on
	// maksimaalne selle elemendi reaindeksist ja veeruindeksist (indeksid
	// algavad nullist).

	public static int[][] maatriks(int n) {

		int maatriks[][] = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i >= j) {
					maatriks[i][j] = i;
				} else {
					maatriks[i][j] = j;
				}
			}
		}
		return maatriks;
	}

	// Sportlase punktisumma arvutatakse üksikkatsetest saadud punktide
	// summana, millest on maha vőetud kahe halvima katse tulemused
	// (üksikkatseid on rohkem kui kaks). Kirjutada Java meetod, mis arvutab
	// punktisumma üksikkatsete tulemuste massiivi pőhjal. Parameetriks
	// olevat massiivi muuta ei tohi.

	public static int skoor(int[] tulemused) {
		Arrays.sort(tulemused);
		int summa = 0;
		for (int i = 2; i < tulemused.length; i++) {
			summa = summa + tulemused[i];
		}
		return summa;
	}

	// Koostage Java meetod etteantud täisarvumaatriksi m veerusummade massiivi
	// leidmiseks (massiivi j-s element on maatriksi j-nda veeru summa).
	// Arvestage, et m read vőivad olla erineva pikkusega.

	public static int[] veergudeSummad(int[][] matrix) {
		int pikim = matrix[0].length;
		for (int i = 0; i < matrix.length; i++) {
			if (matrix[i].length > matrix[0].length) {
				pikim = matrix[i].length;
			}
		}
		
		int[] summad = new int[pikim];
		for (int i = 0; i < matrix.length; i++) { // i tähistab rida
			for (int j = 0; j < matrix[i].length; j++) {
				summad[j] += matrix[i][j];
			}
		}
		
		for (int i = 0; i < summad.length; i++) {
			System.out.println(summad[i]);
	}
//		for (int s : summad) {
//			System.out.println(s);
//		}
		return summad;

	}	
	
}
