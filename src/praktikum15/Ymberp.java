package praktikum15;

//On antud positiivne täisarv n. Kirjutada Java meetod, mis leiab täisarvu, 
// * mis saadakse n kümnendesituses numbrite järjekorra ümberpööramise teel.

public class Ymberp {

	public static void main(String[] args) {
		System.out.println(inverse(12345)); // 4321
	}

	public static int inverse(int n) {
		int backwards = 0;
		int rest = 0;

		do {
			rest = n % 10;
			backwards = backwards * 10 + rest;
			n = n / 10;
		} while (n > 0);

		return backwards;
	}

}
