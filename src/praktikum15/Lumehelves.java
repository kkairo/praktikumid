package praktikum15;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;



public class Lumehelves {
	
	int x, y;
	
	
	public Lumehelves (int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void joonistaMind(GraphicsContext gc) {
		
		
		//gc.fillText("Joonista siia lumehelves", x, y);
		gc.setStroke(Color.BLUE);
		gc.strokeLine(x - 100, y - 100, x + 100, y + 100);
		gc.strokeLine(x - 100, y + 100, x + 100, y - 100);
		gc.strokeLine(x, y + 85, x, y - 85);
		gc.strokeLine(x - 85, y, x + 85, y);
		
	}

}
