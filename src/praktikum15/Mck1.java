package praktikum15;

public class Mck1 {

	public static void main(String[] args) {

		System.out.println(ruutudeSumma(2.2, 1.1));

		System.out.println(valiVahemik(35));

		int[] massiiv = new int[] { 1, -4, 4, 4, -4 };
		System.out.println(posElArv(massiiv));

	}

	// Koostage Java-meetod, mis leiab kahe etteantud reaalarvu ruutude summa.
	public static double ruutudeSumma(double a, double b) {
		double ruutudeSumma = Math.pow(a, 2) + Math.pow(b, 2);
		return ruutudeSumma;
	}

	// Kirjuta funktsioon valiVahemik, mis vőtab ühe int tüüpi argumendi ja
	// tagastab:
	// Arvu 0 kui argument oli väiksem kui 10
	// Arvu 1 kui argument oli vahemikus 10 - 35
	// Arvu 2 kui argument oli suuem kui 35

	public static int valiVahemik(int m) {
		if (m < 10) {
			return 0;
		} else if (m >= 10 && m <= 35) {
			return 1;
		} else {
			return 2;
		}

	}

	// Koostage Java-meetod, mis leiab etteantud massiivi m rangelt positiivsete
	// elementide arvu.
	public static int posElArv(int[] m) {
		int posKokku = 0;
		for (int i = 0; i < m.length; i++) {
			if (m[i] > 0) {
				posKokku = posKokku + 1;
			}
		}
		return posKokku;
	}

	// Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d pőhjal
	// niisuguste elementide arvu, mis on rangelt väiksemad kőigi elementide
	// aritmeetilisest keskmisest (aritmeetiline keskmine = summa /
	// elementide_arv).
	
	public static int allaKeskmise (double[] d){
		double sum = 0;
		for (int i = 0; i < d.length; i++) {
			sum = sum + d[i];
		}
		double keskmine;
		keskmine = sum / (d.length);

		int allaKeskmise = 0;
		for (int j = 0; j < d.length; j++) {
			if (d[j] < keskmine) {
				allaKeskmise = allaKeskmise + 1;
			}
		}
		return allaKeskmise;
	}
	
	// Koostage Java-meetod, mis leiab kahe etteantud reaalarvu
		// absoluutvההrtuste summa.
		public static double absvdeSumma(double a, double b) {
			double summa = Math.abs(a) + Math.abs(b);
			return summa;
		}

		// Koostage Java-meetod, mis teeb kindlaks, kas etteantud tהisarv n on
		// positiivne paaritu arv.
		public static boolean posPaaritu(int n) {
			if (n > 0 && n % 2 != 0) {
				return true;
			} else {
				return false;
			}
		}
		// Koostage Java-meetod, mis leiab etteantud massiivi m rangelt negatiivsete
		// elementide arvu.
		public static int negElArv(int[] m) {
			int negKokku = 0;
			for (int i = 0; i < m.length; i++) {
				if (m[i] < 0) {
					negKokku = negKokku + 1;
				}
			}
			return negKokku;
		}
		
}
