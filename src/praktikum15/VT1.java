package praktikum15;

public class VT1 {

	 public static void main (String[] args) {
	        System.out.println (absvdeSumma (0., 0.));
	        System.out.println(absvdeSumma(-3, -2));
	        System.out.println(negPaaritu(-2));

	        System.out.println(nullideArv(new int[]{1,3,0,5,0,6,0}));

	        System.out.println (allaKeskmise (new double[]{1,5,7,9,3}));

	        System.out.println(posPaarisarv(4));

	        System.out.println(ruutudeSumma(new int[]{1, 3, 0, 5, 0, 6, 0}));

	        System.out.println (summaAbsv (-4.1, -2));
	    }

	    public static double summaAbsv (double a, double b) {
	        return Math.abs(a) + Math.abs(b);
	    }

	    public static double absvdeSumma (double a, double b) {
	        return Math.abs(a) + Math.abs(b);
	    }

	    // public staatiline meetod, mis tagastab int tüübi ja saab sisendiks int massiivi m .
	    //nimi "m" kasutatav vaid meetodi sees
	    public static int ruutudeSumma(int[] m){
	        //summa loendur, mille algväärtus on null.
	        int summa = 0;
	        //loe: iga massiivi "m" nn sahtli, mille nimeks panen tsükli sees kasutuseks "element", tee seda:
	        for(int element : m){
	            //liida olemasolevale summale juurde elemendi teise astme väärtus.
	            // Math.pov(<astendatav>, <astendaja>
	            summa += Math.pow(element, 2);
	        }
	        //tagastan lõpliku summa pärast kõigi elemendide ruutude liitmist.
	        return summa;
	    }

	    public static boolean negPaaritu (int n) {
	        if (n < 0 && n % 2 != 0){
	            return true;
	        }
	        else{
	            return false;
	        }
	    }


	    public static int nullideArv (int[] m) {
	        int nullideHulk = 0;
	        for (int i = 0; i < m.length; i++){
	            if (m[i] == 0){
	                nullideHulk ++;
	            }
	        }

	        return nullideHulk;
	    }

	    public static int posSumma (int[] m) {
	        int posElemendid = 0;
	        for (int i = 0; i < m.length; i++){
	            if (m[i] > 0){
	                posElemendid ++;
	            }
	            else continue;
	        }

	        return posElemendid;
	    }

	    public static int allaKeskmise (double[] d) {
	        double keskmine = 0;
	        int elementideHulk = 0;
	        //get average
	        for (int i = 0; i < d.length; i++){
	            keskmine += d[i];
	        }
	        keskmine = keskmine / d.length;
	        //get count
	        for (int i = 0; i < d.length; i++){
	            if (d[i] < keskmine){
	                elementideHulk ++;
	            }
	        }
	        System.out.println(keskmine);
	        return elementideHulk;
	    }

	    public static boolean posPaarisarv(int n){
	        if (n > 0 && n % 2 == 0){
	            return true;
	        }
	        else{
	            return false;
	        }
	    }	
	
}
