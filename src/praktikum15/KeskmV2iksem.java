package praktikum15;

//Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d põhjal niisuguste elementide arvu, 
//mis on rangelt väiksemad kõigi elementide aritmeetilisest keskmisest 
// (aritmeetiline keskmine = summa / elementide_arv). 

public class KeskmV2iksem {

	public static void main(String[] args) {
		System.out.println(allaKeskmise(new double[] { 0. }));
		System.out.println(allaKeskmise(new double[] { 0., 5., 3., 2. }));
	}

	public static int allaKeskmise(double[] d) {
		int mituTykki = 0;
		double summa = 0.;
		double keskmine = 0.;

		for (int i = 0; i < d.length; i++) {
			summa = summa + d[i];
		}
		keskmine = summa / (d.length);

		for (int i = 0; i < d.length; i++) {
			if (d[i] < keskmine) {
				mituTykki = mituTykki + 1;
			}

		}

		return mituTykki;
	}

}
