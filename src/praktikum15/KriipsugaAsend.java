package praktikum15;

//Koostada Java meetod, mis asendab parameetrina etteantud sõnes s kõik väiketähed märgiga '-'. 
// Lahendus peab kasutama tsüklit.

public class KriipsugaAsend {

	public static void main(String[] args) {
		String s = "Tere, TUDENG, tore ARVUTI sul!";
		String t = asenda(s); // "T---, TUDENG, ---- ARVUTI ---!"
		System.out.println(s + " > " + t);
	}

	public static String asenda(String s) {

		String result = "";

		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (Character.isLowerCase(ch)) {
				result += "-";
			} else {
				result += ch;
			}

		}

		return result;
	}

}
