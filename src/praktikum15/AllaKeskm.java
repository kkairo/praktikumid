package praktikum15;

public class AllaKeskm {

//Koostage Java meetod, mis leiab etteantud reaalarvude massiivi 
//d põhjal niisuguste elementide arvu,
//mis on rangelt väiksemad kõigi elementide aritmeetilisest keskmisest 
//	(aritmeetiline keskmine = summa / elementide_arv).

	    public static void main(String[] args){
	        double[] massiiv = {1,5,7,3,8,9,4};
	        int count = allaKeskmise(massiiv);
	        System.out.println(count);
	    }

	    public static int allaKeskmise (double[] d){
	        int belowAvCount = 0;
	        double average = 0;
	        double sum = 0;
	        
	        for(int i=0; i<d.length;i++){
	        sum += d[i];
	    }
	        System.out.println(sum);
	        //get average value
	        for (double el : d){
	            sum += el;
	        }
	        average = sum / d.length;

	        //get count
	        for(double el : d){
	            if(el < average){
	                belowAvCount++;
	            }
	        }
	        return belowAvCount;
	    }
	
}
