package praktikum13;

import java.util.ArrayList;

public class Aritmeetiline2 {

	public static void main(String[] args) {
		
		String kataloogitee = FailistLugemine.class.getResource(".").getPath();
		ArrayList<String> failiSisu = FailistLugemine.loeFail(kataloogitee + "numbrid.txt");
		System.out.println(failiSisu);
		
		double summa = 0;
		int vigaseidRidu = 0;
		for (String rida : failiSisu) {
			double nr;
			try {
				nr = Double.parseDouble(rida);
				summa += nr;
			
			} catch (NumberFormatException e) {
				System.out.println("Vigane rida (ei arvesta arvutuses):" + rida);
				vigaseidRidu++;
			}
			
		}
		System.out.println(summa);
		double keskmine = summa / (failiSisu.size() - vigaseidRidu);
		System.out.println("Keskmine on: " + keskmine);
		
		}
	}


