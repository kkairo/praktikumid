package praktikum13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

public class FailistLugemine {
	
	public static ArrayList<String> loeFail(String failinimi) {
		
		ArrayList<String> read = new ArrayList<String>();
		
		// otsime samast kataloogist kala.txt-nimelist faili
		File file = new File(failinimi);
		//ArrayList<String> kalad = new ArrayList<String>();
	
		try {
			// avame faili lugemise jaoks
			BufferedReader in = new BufferedReader(new FileReader(file));
			String rida;
			
			// loeme failist rida haaval
			while ((rida = in.readLine()) != null) {
				read.add(rida);
			
			}
		}
		catch (FileNotFoundException e) {
			System.out.println("Faili ei leitud: \n" + e.getMessage());
		}
		catch (Exception e) {
			System.out.println("Error, jee, mingi muu error: " + e.getMessage());
		}
		return read;
	}
	
	public static void main(String[] args) {
	    
	    // punkt tähistab jooksvat kataloogi
	    String kataloogitee = FailistLugemine.class.getResource(".").getPath();
	    ArrayList<String> failiSisu = loeFail(kataloogitee + "kalad.txt");
		
	    Collections.sort(failiSisu, String.CASE_INSENSITIVE_ORDER);
    	System.out.println(failiSisu);
	    
	}
}
