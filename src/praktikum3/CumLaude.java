package praktikum3;

import lib.TextIO;

//Kirjutada Cum-Laude detektor.
//Programm küsib kasutajalt kaalutud keskhinde ja lõputöö hinde.
//Kui keskhinne on suurem kui 4.5 ja lõputöö hinne on 5 siis ütleb "Jah saad cum laude diplomile!". Muul juhul ütleb "Ei saa!"
//NB! Programm ei tohi aktsepteerida vigaseid hindeid (üle 5-e, negatiivseid jms) Vigase hinde puhul tuleb anda veateade.

public class CumLaude {

	public static void main(String[] args) {

		System.out.println("Mis on sinu keskmine hinne?");
		double keskmineHinne = TextIO.getlnDouble();
		
		if (keskmineHinne > 5 || keskmineHinne < 0) {
			System.out.println("Ei saa olla!");
			return;
		}	
		
		System.out.println("Mis on sinu lõputöö hinne?");
		int l6put66 = TextIO.getlnInt();
		
		if (l6put66 > 5 || l6put66 < 0) {
			System.out.println("Ei saa olla!");

		} else if (keskmineHinne > 4.5 && l6put66 == 5) {
			System.out.println("Palju õnne - sa saad cum laude!");
		} else {
			System.out.println("Cum laudet kahjuks ei saa.");

		}

	}

}
