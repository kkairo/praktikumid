package praktikum3;

import lib.TextIO;

//Programm küsib kasutajalt kaks vanust.
//Kui vanuste vahe on rohkem kui viis aastat ütleb midagi krõbedat.
//Kui vanuste vahe on rohkem kui kümme aastat ütleb midagi veel krõbedamat.
//Kui vanuste vahe jääb alla viie aasta, ütleb, et sobib.
//NB! Programmi töö ei tohi sõltuda vanuste sisestamise järjekorrast!

public class Tehisintellekt {

	public static void main(String[] args) {
		
		System.out.println("Palun sisesta kaks vanust: ");
		int vanus1 = TextIO.getlnInt();
		int vanus2 = TextIO.getlnInt();
		
		int vanusevahe = Math.abs(vanus1 - vanus2);
		
		if (vanusevahe > 10) {
			System.out.println("Natuke mööda! Vanusevahe on suurem kui 10");			
		} 
		else if (vanusevahe > 5) {
			System.out.println("Veel rohkem mööda!Vanusevahe on suurem kui 5");
		} 
		else {
			System.out.println("Sobib!");
		}
	}
}
