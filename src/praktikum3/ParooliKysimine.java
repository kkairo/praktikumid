package praktikum3;

import lib.TextIO;

public class ParooliKysimine {

	public static void main(String[] args) {
		
		String oigeParool = "saladus";

		
		System.out.println("Sisesta parool: ");
		String kasutajaSisestus = TextIO.getlnString();
		
		if 	(oigeParool.equals(kasutajaSisestus)) {
			System.out.println("Õige parool!");
			
		} else {
			System.out.println("Vale parool!");
		}

	}

}

//String parool = "ParooL123";
//String sisend;
//
///*
// * while ts�kkel on kirjutatud nii, et kui if ts�klis tuleb t�ene
// * v��rtus, siis while ts�kkel katkestatakse (break), st sisestatud on
// * �ige parool ning parooli sisestamist uuesti ei k�sita
// */
//
//while (true) {
//
//	// Parooli k�simine ja kontrollimine
//	System.out.println("Sisestage parool: ");
//	sisend = TextIO.getlnWord();
//
//	if (sisend.equals(parool)) {
//		System.out.println("Olete sisse loginud!");
//		break;
//	} else {
//		System.out.println("Parool on vale!");
//	}
//}
