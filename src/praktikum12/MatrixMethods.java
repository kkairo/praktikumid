package praktikum12;

public class MatrixMethods {

	public static void main(String[] args) {

		int[] massiiv = new int[] { 2, 3, 4, 2, 3, 3 };

		int[][] maatriks = new int[][] { { 1, 5, 3, 5 }, { 1, 9, 3, -3 }, { -1, 7, 2, 1 }, { -4, -5, 10, 2 } };
		// tryki(ridadeMaksimumid(maatriks));
		// tryki(ridadeSummad(maatriks));
		// System.out.print(korvalDiagonaaliSumma(maatriks));
		// System.out.print(miinimum(maatriks));
		 tryki(transponeeri(maatriks));
	}

	// Kirjutada meetod, mis trükib ekraanile ühel real parameetrina etteantud
	// ühemőőtmelise täisarvumassiivi elemendid

	public static void tryki(int[] massiiv) {
		for (int i = 0; i < massiiv.length; i++) {
			System.out.print(massiiv[i] + " ");
		}
	}

	// Kirjutada meetod, mis trükib ekraanile parameetrina etteantud
	// kahemőőtmelise massiivi (maatriksi)

	public static void tryki(int[][] maatriks) {
		for (int i = 0; i < maatriks.length; i++) {
			tryki(maatriks[i]);
			System.out.println();
		}
	}

	// Arvutada maatriksi iga rea elementide summa

	public static int[] ridadeSummad(int[][] maatriks) {

		int[] reaSummaMassiiv = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			int sum = 0;
			for (int j = 0; j < maatriks[i].length; j++) {
				sum = sum + maatriks[i][j];
			}
			reaSummaMassiiv[i] = sum;
		}
		return reaSummaMassiiv;

	}

	// Arvutada kőrvaldiagonaali elementide summa (kőrvaldiagonaal on see, mis
	// jookseb ülevalt paremast nurgast alla vasakusse nurka)

	public static int korvalDiagonaaliSumma(int[][] maatriks) {
		int sum = 0;
		for (int i = 0; i < maatriks.length - 1; i++) {
			sum = sum + maatriks[i][i + 1];
		}
		return sum;
	}

	// Leida iga rea suurim element

	public static int[] ridadeMaksimumid(int[][] maatriks) {
		int[] reaMaxMassiiv = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			int reaMax = maatriks[i][0];
			for (int j = 0; j < maatriks[i].length; j++) {
				if (maatriks[i][j] >= reaMax) {
					reaMax = maatriks[i][j];
					reaMaxMassiiv[i] = reaMax;
				}
			}
		}
		return reaMaxMassiiv;
	}

	// Leida kogu maatriksi väikseim element

	public static int miinimum(int[][] maatriks) {
		int[] reaMinMassiiv = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			int reaMin = maatriks[i][0];
			for (int j = 0; j < maatriks[i].length; j++) {
				if (maatriks[i][j] <= reaMin) {
					reaMin = maatriks[i][j];
					reaMinMassiiv[i] = maatriks[i][j];
				}
			}
		}
		int min = reaMinMassiiv[0];
		for (int k = 0; k < reaMinMassiiv.length; k++) {
			if (reaMinMassiiv[k] <= min) {
				min = reaMinMassiiv[k];
			}
		}
		return min;

	}

	// Kirjutada programm, mis genereerib parameetritena etteantud suurusega
	// maatriksi, kus iga element on rea ja veeruindeksi summa kahega jagamise
	// jääk. Indeksid algavad nullist

	public static int[][] kahegaJaakMaatriks(int ridu, int veerge) {
		int[][] matrix = new int[ridu][veerge];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = (i + j) % 2;
			}
		}
		return matrix;
	}

	// Transponeerida allpool toodud maatriks ja trükkida tulemus ekraanile

	public static int[][] transponeeri(int[][] maatriks) {
		int[][] transMat = new int[maatriks[0].length][maatriks.length];
		for (int i = 0; i < maatriks[0].length; i++) {
			for (int j = 0; j < maatriks.length; j++) {
				transMat[i][j] = maatriks[j][i];
			}
		}
		return transMat;
	}	
	
}
