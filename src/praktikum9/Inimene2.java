package praktikum9;

import lib.TextIO;

public class Inimene2 {

	String nimi;
	int vanus;

	// suure algustähega, sest on konstruktor. kui loome new käsuga uue objekti,
	// see ongi nagu meetodi välja kutsumine, parameetrite andmisel kutsutakse
	// välja konstruktor meetod
	public Inimene2(String nimi, int vanus) {
//		System.out.println("ole konstruktor");
		this.nimi = nimi;
		this.vanus = vanus;
	}

	// Võrdleb inimesi teise inimesega
	public boolean equals(Inimene teine) {
		return teine.vanus == this.vanus && this.nimi.equals(teine.nimi);
	}

	public void tervita() {
		TextIO.putln("Tere, minu nimi on " + nimi + ", olen " + vanus + "-aastane.");
	}

	// kutsutkase välja, kui kutsukse välja systemoutiga. Override kirjutab üle
	// meetodi, mida see klass laiendab või täiendab
	@Override
	public String toString() {
		return nimi + " " + vanus;
	}
}
