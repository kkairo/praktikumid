package praktikum9;

public class Tulpdiagramm {

	int arv;

	public Tulpdiagramm(int arv) {
		this.arv = arv;
	}

	public void diagramm() {
		System.out.printf("%2d ", arv);
		for (int i = 0; i < arv; i++) {
			System.out.print("x");
		}
		System.out.println();
	}
	
	
}
