package praktikum9;

import praktikum1.TextIO;

public class Inimene {
	
	String nimi;
	int vanus;
	
	public Inimene(String nimi, int vanus) {//konstruktor - kutsutakse välja hetkel kui loodakse uus meetod
		this.nimi = nimi;
		this.vanus = vanus;
	}
	
	public boolean equals(Inimene teine) {
		return teine.vanus == this.vanus
				&&this.nimi.equals(teine.nimi);
	}

	public void tervita () {
		TextIO.putln("Tere, minu nimi on " + nimi + ", olen " + vanus + " -aastane.");
	}
	
	@Override
	public String toString() {//kirjutame üle meetodi
		return nimi + " " + vanus;
	}
}
