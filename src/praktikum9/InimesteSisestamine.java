package praktikum9;

import java.util.ArrayList;

import lib.TextIO;

public class InimesteSisestamine {

	public static void main(String[] args) {

		// String nimi = new String("Mati");

		// Inimene keegi = new Inimene("Kati", 23);//meetodi välja kutsumine

		// Inimene keegiVeel = new Inimene ("Juri", 23);

		// System.out.println(keegi);
		String nimi = "";
		int vanus = 0;
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();

		while (true) {
			
			System.out.print("Sisesta nimi: ");
			nimi = TextIO.getlnString();
			// Kui nimeks panna "exit", siis lõpetatakse nimeda küsimine
			if (nimi.equals("exit")) {
				break;
			}
			System.out.print("Sisesta selle inimese vanus: ");
			vanus = TextIO.getlnInt();
			inimesed.add(new Inimene(nimi, vanus));
		}

		for (Inimene inimene : inimesed) {
			// Java kutsub v�lja Inimene klassi toString() meetodi
			System.out.println(inimene);
			inimene.tervita();
		}

	}
		}
			
			
			
//			System.out.println("Sisesta nimi ja vanus");
//			String nimi = TextIO.getlnString();
//			if (nimi.equals(""))
//				break;
//			int vanus = TextIO.getlnInt();
//			Inimene keegi = new Inimene(nimi, vanus);
//			inimesed.add(keegi);
//		}
//
//		for (Inimene inimene : inimesed) {// kui tahad k6iki elemente välja
//											// trükkida järjest
//			// Java kutsub välja Inimene klassi toString() meetodi
//
//			inimene.tervita();
//		}
//	}
//}
