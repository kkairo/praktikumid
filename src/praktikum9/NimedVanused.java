package praktikum9;

import java.util.ArrayList;

import lib.TextIO;

public class NimedVanused {

	// Kirjutada programm, mis küsib kasutajalt vaheldumisi inimeste nimesid ja
	// vanuseid. Kasutaja sisestatud andmed pane näiteprogrammide hulgas
	// olevasse "Inimene" klassi. Kutsu ükshaaval välja iga "Inimene" tüüpi
	// objekti tervita() - meetod.

	public static void main(String[] args) {

		String nimi = "";
		int vanus = 0;
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		while (true) {

			System.out.print("Sisesta nimi: ");
			nimi = TextIO.getlnString();
			if (nimi.equals("exit")) {
				break;
			}
			System.out.print("Sisesta selle inimese vanus: ");
			vanus = TextIO.getlnInt();
			inimesed.add(new Inimene(nimi, vanus));
		}
		
		for (Inimene inimene : inimesed) {
			// Java kutsub välja Inimene klassi toString() meetodi
			System.out.println(inimene);

		}
		// Inimene lause = new tervita();
		
		// ei oska teha, ei saa ülesandest aru
		
	}

}
