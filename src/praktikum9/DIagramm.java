package praktikum9;

import java.util.ArrayList;

import lib.TextIO;

public class DIagramm {

	public static void main(String[] args) {

		int arv = 0;
		ArrayList<Tulpdiagramm> arvud = new ArrayList<Tulpdiagramm>();
		while (true) {

			System.out.print("Sisesta arv: ");
			arv = TextIO.getlnInt();
			if (arv > 0) {
				arvud.add(new Tulpdiagramm(arv));
			} else if (arv < 0) {
				System.out.println("Sisestatud arv peab olema positiivne!");
			} else if (arv > 80) {
				arv = 80;
			} else if (arv == 0) {
				break;
			}
		}

		for (Tulpdiagramm element : arvud) {
			element.diagramm();

		}
	}

}
