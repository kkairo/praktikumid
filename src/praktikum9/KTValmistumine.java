package praktikum9;

import java.util.Arrays;

public class KTValmistumine {

	public static void main(String[] args) {

		/**
		 * ---------------------------------------------------------------------
		 * MEETODITE VÄLJAKUTSUMINE JA KONTROLL
		 * ---------------------------------------------------------------------
		 */

		// ** Meetodi 1 väljakutsumine ja maatriksi printimine

		// Defineerime maatriksi laiuse ja kőrguse
		int width = 3;
		int heigth = 5;

		// Defineerima meetodi poolt genereeritud maatriksi
		int[][] maatriks = generateMatrix(width, heigth);

		// For-tsükli ablil prindime maatriksi nii, et see oleks ka loetav ja
		// korralik. Printimise for-tsykli idee on analoogne, mis on kirjeldatud
		// meetodis 1
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				System.out.print(maatriks[i][j] + "\t");
			}
			System.out.println(); // et read oleksid üksteise all
		}

		System.out.println("-----------------------------------------------");
		// ** Meetod 2 väljakutsumine ja vastava massiivi printimine

		for (int arv : maxMedianRow(maatriks)) {
			System.out.print(arv + "\t");
		}

		System.out.println();
		System.out.println("-----------------------------------------------");
		// ** Meetod 3 väljakutsumine ja vastava massiivi printimine

		System.out.println(maxAndMinDifference(maatriks));

	}

	/**
	 * -------------------------------------------------------------------------
	 * M E E T O T I D
	 * -------------------------------------------------------------------------
	 */

	// ******** 1. MEETOD ********
	// Kirjuta meetod, mis genereerib sellise kahemőőtmelise massiivi, mille
	// elemendid on suvalised numbrid vahemikus 0 kuni 100. Tagastatava massiivi
	// suurus antakse meetodile parameetritega ette. (PS! meetodi ülesandeks
	// pole maatriks välja printida)
//	public static int[][] generateMatrix(int width, int heigth) {
//
//		int[][] matrix = new int[heigth][width];

		// Sisemine for-tsykkel kirjutab elemendid ritta, kus maatriksi vastava
		// rea elementide arvu saab leida järgmiselt: array[i].length, kus i on
		// vastava rea number. Väline for-tsykkel kordab sisemist tsyklit nii
		// palju kordi, kui maatriksis on määratud ridasid, mille arvu saab
		// leida array.length.
//		for (int i = 0; i < matrix.length; i++) {
//			for (int j = 0; j < matrix[i].length; j++) {
//				matrix[i][j] = pr06.Meetodid.suvalineT2isarvVahemikus(0, 100);
//			}
//		}
//		return matrix;
//
//	}

	// ******** 2. MEETOD ********
	// Kirjuta meetod, mis arvutab kahemőőtmelise massiivi rea elementide
	// mediaani ning tagastab suurima mediaaniga rea elemendid.
	// MÄRKUS: lihtsuse mőttes eeldan, et maatriksis on kőik read ikka ühe
	// pikkused

	public static int[] maxMedianRow(int[][] matrix) {

		// Esimesena teisendame int tüüpi maatriksi uueks double tüüpi
		// maatriksiks (dblMatrix), sest mediaanid vőivad olla ka double tüüpi
		double[][] dblMatrix = new double[matrix.length][matrix[0].length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				dblMatrix[i][j] = (double) matrix[i][j];
			}
		}

		// Sorteerime selle maatriksi, et kőikidel ridadel on elemendid suuruse
		// järjekorras
		for (int i = 0; i < dblMatrix.length; i++) {
			Arrays.sort(dblMatrix[i]);
		}

		// Defineerime double tüüpi massiivi, kuhu paneme kőikide ridade
		// mediaanväärtused. Paaritu arvu elementide korral sorteerirud rea
		// mediaan väärtus keskmine element (väärtus kohal keskarv =
		// sortMatrix.length/2). Paarisarvu elementide korral tuleb mediaani
		// leidmiseks liita elemendid kohal keskArv-1 ja keskArv ning jagada
		// nende summa kahega (see vőib tulla komaga arv ehk double väärtus!!),
		// st mediaan ei pruugi olla enam täisarv.
		double[] mediaanid = new double[dblMatrix.length];
		for (int i = 0; i < dblMatrix.length; i++) {
			int keskArv = dblMatrix[i].length / 2;
			if (dblMatrix[i].length % 2 != 0) {
				mediaanid[i] = dblMatrix[i][keskArv];
			} else {
				mediaanid[i] = (dblMatrix[i][keskArv - 1] + dblMatrix[i][keskArv]) / 2;
			}
		}

		// Leiame mediaanide massiivi suurima mediaani. Selleks kasutame
		// for-tsüklit (PS! enne tsüklit ütleme, et suurim mediaan on mediaanide
		// massiivis kohal 0). Samuti paneme tähele, et me saame sellisel kujul
		// for-tsüklit kasutada, sest mediaanide väärtusi on sama palju kui
		// maatriksi ridasid. If-lausega leiame selle massiivi indeksi
		// maxMedRida, kus asub suurima väärtusega mediaan. maxMedRida on
		// ühtlasi ka maatriksi rea number, kus asub suurim mediaan.
		double max = mediaanid[0];
		int maxMedRida = 0;
		for (int i = 0; i < mediaanid.length; i++) {
			if (mediaanid[i] > max) {
				maxMedRida = i;
			}
		}
		return matrix[maxMedRida];
	}

	// ******** 3. MEETOD ********
	// Kirjuta meetod, mis tagastab kahemőőtmelise massiivi suurima ja väikseima
	// elemendi vahe.

	public static int maxAndMinDifference(int[][] matrix) {

		// Oletame, et esialgu on suurimad väärtused:
		int max = matrix[0][0];
		int min = matrix[0][0];

		// For-tsükkel maksimumi leidmiseks
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (matrix[i][j] > max) {
					max = matrix[i][j];
				}
			}
		}

		// For-tsükkel miinimumi leidmiseks
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (matrix[i][j] < min) {
					min = matrix[i][j];
				}
			}
		}

		// Maksimumi ja miinimumi vahe
		int vahe = max - min;
		return vahe;
	}	
	
}
