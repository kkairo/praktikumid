package praktikum9;

public class SuurimElement {

public static void main(String[] args) {
		
		// Kasutame praktikumis 7 loodud meetodeid

		// Massiivist maksimumi leidmine
		int[] massiiv = { 1, 3, 6, 7, 8, 3, 5, 7, 21, 3 };
		int maxMassiiv = praktikum7.Meetodid.massiivMax(massiiv);
		System.out.println(maxMassiiv);

		// Maatriksist maksimumi leidmine
		int[][] neo = { { 1, 3, 6, 7 }, { 2, 3, 3, 1 }, { 17, 4, 5, 0 }, { -20, 13, 16, 17 } };
		int maxMaatriks = praktikum7.Meetodid.maatriksMax(neo);
		System.out.println(maxMaatriks);
	}	
	
}
