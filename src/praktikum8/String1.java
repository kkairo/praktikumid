package praktikum8;

import praktikum1.TextIO;

public class String1 {
	
	public static void main(String[] args) {
		String s;
		int i;
		
		System.out.print("Sisesta midagi: ");
		s = TextIO.getlnString();
		
		for(i = 0; i < s.length(); i = i + 1) {
			System.out.print(s.charAt(i) + " ");
		}
		System.out.println();
	}

}
