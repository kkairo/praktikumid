package praktikum8;

import lib.TextIO;

public class Palindroom {

	// Kirjutada programm, mis k2seb kasutajalt s6na ja kontrollib, kas see
		// on edaspidi ja tagurpidi lugedes sama (palindroom).

		public static void main(String[] args) {


			// S6na sisestamine
			System.out.print("Sisesta sőna: ");
			String s6na = TextIO.getlnString();

			// SĂľna tagurpidi pööramine (PS! tagurpidi string peab defineerima enne
			// for-tsyklit kui tühja stringina
			String tagurpidi = "";
			for (int i = s6na.length() - 1; i >= 0; i--) {
				tagurpidi = tagurpidi + s6na.charAt(i);
			}

			// S6na ja tagurpidi s6na v6rdlemine
			if (s6na.equals(tagurpidi)) {
				System.out.println("Sőna " + s6na + " on palindroom!");
			} else {
				System.out.println("Sőna " + s6na + " ei ole palindroom!");
			}

		}	
	
}
