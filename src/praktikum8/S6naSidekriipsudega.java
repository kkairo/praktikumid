package praktikum8;

import lib.TextIO;

public class S6naSidekriipsudega {

	public static void main(String[] args) {


		// S6na sisestamine ja t2htede suureks muutmine
		System.out.print("Sisesta sőna: ");
		String s6na = TextIO.getlnString();
		String suuredT2hed = s6na.toUpperCase();

		// Prindime s6na esimese t2he (PS! 2ra kasuta println)
		System.out.print(suuredT2hed.charAt(0));
		
		// For-tsykliga prindime v2lja vahepealsed t2hed
		for (int i = 1; i < s6na.length() - 1; i++) {
			System.out.print("-" + suuredT2hed.charAt(i));
		}
		
		// Prindime s6na viimase t2he
		System.out.print("-" + suuredT2hed.charAt(s6na.length() - 1));

	}	
}

//public static Scanner userInput = new Scanner(System.in);
//
//public static void main(String[] args){
//    String s6na = "";
//    System.out.print("Sisesta s6na: ");
//    s6na = getName();
//
//    //creating string array of letters:
//    String[] s6naArray = s6na.split("");
//
//    //printing:
//    for (int i = 0; i < s6naArray.length; i++){
//        System.out.print(s6naArray[i].toUpperCase());
//        //if not last letter, print "-" aswell
//        if (i + 1 != s6naArray.length){
//            System.out.print("-");
//        }
//    }
//    userInput.close();
//}
//
//private static String getName(){
//    String name = "";
//    try {
//        name = userInput.next();
//    }catch (InputMismatchException e){}
//    return name;
//}