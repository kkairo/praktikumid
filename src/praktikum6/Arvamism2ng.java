package praktikum6;

import lib.TextIO;

//Arvuti "mõtleb" ühe arvu ühest sajani.
//Küsib kasutajalt, mis number oli.
//Kui kasutaja vastab valesti, siis ütleb kas arvatud number oli suurem või väiksem.
//Programm küsib vastust senikaua kuni kasutaja numbri õigesti ära arvab.

public class Arvamism2ng {

	public static void main(String[] args) {

		int arvutiArv = suvalineArv(1, 100);
		System.out.println("Sisesta mingi suvaline arv vahemikus " + 1 + " kuni " + 100);

		while (true) {

			int kasutajaArvas = TextIO.getlnInt();
			if (arvutiArv == kasutajaArvas) {
				System.out.println("Arvasid ära!");
				break;
			} else if (arvutiArv > kasutajaArvas) {
				System.out.println("See arv on suurem");
			} else {
				System.out.println("See arv on väiksem");
			}		
		}
	}

	public static int suvalineArv(int min, int max) {
		int vahemik = max - min + 1;
		return (int) (Math.random() * vahemik) + min;
	}


//int arvutiArv = (int) (Math.random()*100 + 1);
//int kasutajaPakkumine = 0;
//
//while (arvutiArv != kasutajaPakkumine){
//    kasutajaPakkumine = KysiArvu(1, 100);
//    if (kasutajaPakkumine < arvutiArv){
//        System.out.println("Sinu pakkumine on v2iksem kui arvuti arv");
//    }
//    if (kasutajaPakkumine > arvutiArv){
//        System.out.println("Sinu pakkumine on suurem kui arvuti arv");
//    }
//    if (kasutajaPakkumine == arvutiArv){
//        System.out.println("Arvasid ära!");
//    }
//}
//}
//
public static int KysiArvu(int _min, int _max){
int kasutajaPakkumine = _min -1;
while (kasutajaPakkumine < _min || kasutajaPakkumine > _max){
    System.out.println("Sisesta arv vahemikus " + _min + " ja " + _max);
    kasutajaPakkumine = TextIO.getlnInt();
}
return kasutajaPakkumine;
}
}