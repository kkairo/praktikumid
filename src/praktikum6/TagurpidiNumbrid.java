package praktikum6;

import lib.TextIO;

//Kirjutada programm, mis küsib kasutajalt 10 arvu 
//ning trükib nad seejärel vastupidises järjekorras ekraanile

public class TagurpidiNumbrid {

	public static void main(String[] args) {
	// Massiivi defineerimine
	System.out.println("Sisesta suvalises järjekorras kümme arvu ");
	int[] arvud = new int[10];

	// Arvude küsimine ehk massiivi täitmine
	for (int i = 0; i < arvud.length; i++) {
		System.out.print("Sisesta arv: ");
		arvud[i] = TextIO.getlnInt();
	}

	// Sisestatud arvude järjekorra vastupidiseks muutumine
	for (int j = arvud.length - 1; j >= 0; j--) {
		System.out.printf("%2d", arvud[j]);
	}
}
}
