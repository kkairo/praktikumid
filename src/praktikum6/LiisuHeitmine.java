package praktikum6;

import java.util.Scanner;

//Kirjutada liisu heitmise programm. Programm kysib 10 nime, valib neist suvalise 
//nime (randomiga) ja trükib välja. Täiendus: Küsida esimese asjana inimeste arv.

public class LiisuHeitmine {

	public static void main(String[] args){

        String [] nimed = new String[10];
        Scanner sc = new Scanner(System.in);
        int randomNum = (int) (Math.random() * 10);

        System.out.println("Sisesta 10 nime: ");

        for (int i = 0; i < 10; i++){
            System.out.print(i + 1 + ") ");
            nimed[i] = sc.nextLine();
        }

        System.out.println("Valituks osutus: " + nimed[randomNum]);

        sc.close();
    }	
}
