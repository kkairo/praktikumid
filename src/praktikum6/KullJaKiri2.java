package praktikum6;

import praktikum5.Vahemik;

//Kirjutada panustega kulli ja kirja mäng.
//Kasutajale antakse 100 raha.
//Küsitakse panuse suurust, maksimaalne panus on 25 raha.
//Visatakse münt.
//Kui tuli kiri, saab kasutaja panuse topelt tagasi.
//Kui tuli kull, ei saa ta midagi.
//Mäng kestab seni, kuni kasutajal raha otsa saab.
//NB! Mõistlik on kasutajale raha jääki vahel näidata ka.

public class KullJaKiri2 {

	public static void main(String[] args) {

		int kasutajaRaha = 100;
		
		while (kasutajaRaha > 0) {
			System.out.println("Sul on " + kasutajaRaha + " raha.");
			int maxPanus = Math.min(kasutajaRaha, 25);
			System.out.println("Palun sisesta panus (max " + maxPanus + ").");
			int panus = Vahemik.kasutajaSisestus(1, maxPanus);	//meetod klassist Vahemik
			kasutajaRaha -= panus;
			
			int kullV6iKiri = Arvamism2ng.suvalineArv(0, 1);
			
			if (kullV6iKiri == 0) { // Kull
				System.out.println("Võitsid, saad topelt raha tagasi!");
				kasutajaRaha += panus * 2;
			} else { // Kiri
				System.out.println("Kaotasid...");
			}
			
		}
		
		
	}

}
