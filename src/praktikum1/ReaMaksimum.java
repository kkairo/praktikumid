package praktikum1;

public class ReaMaksimum { //Koostage Java meetod etteantud täisarvumaatriksi m reamaksimumide massiivi leidmiseks (massiivi i-s element on maatriksi i-nda rea suurima elemendi väärtus). Read võivad olla erineva pikkusega. 

	public static void main(String[] args) {
		
		int[] res = reaMaxid (new int[][] { { 1, 2, 3 }, { 4, 5, 6 }, { -3 } }); // {3,
		//System.out.println(reaMaxid); ????
	}

	public static int[] reaMaxid(int[][] m) {

		int[] vastus = new int[m.length];

		for (int l = 0; l < m.length; l++) {

			vastus[l] = m[l][0];
			for (int i = 0; i < m[l].length; i++) {
				if (vastus[l] < m[l][i]) {
					vastus[l] = m[l][i];
				}
			}
		}
		return vastus;

	}

}
