package praktikum1;

/**
 * Koostada Java meetod, mis asendab parameetrina etteantud sõnes s kõik
 * numbrid märgiga '#'. Write a Java method to replace all numbers in a
 * given string s by symbol '#'.
 * 
 * public static String asenda (String s)
 */

public class Asendamine {

	public static void main(String[] args) {
		String s = "Tere, 1234 ja 5678";
		String t = asenda(s); // "Tere, #### ja ####"
		System.out.println(s + " --> " + t);
	}

	public static String asenda(String s) {
		
		String[] jupitatud = s.split("");
		
		for(int i = 0; i < s.length()+1; i++){
			
			if(jupitatud[i].equals("0")){
				jupitatud[i] = "#";
			}
			if(jupitatud[i].equals("1")){
				jupitatud[i] = "#";
			}
			if(jupitatud[i].equals("2")){
				jupitatud[i] = "#";
			}
			if(jupitatud[i].equals("3")){
				jupitatud[i] = "#";
			}
			if(jupitatud[i].equals("4")){
				jupitatud[i] = "#";
			}
			if(jupitatud[i].equals("5")){
				jupitatud[i] = "#";
			}
			if(jupitatud[i].equals("6")){
				jupitatud[i] = "#";
			}
			if(jupitatud[i].equals("7")){
				jupitatud[i] = "#";
			}
			if(jupitatud[i].equals("8")){
				jupitatud[i] = "#";
			}
			if(jupitatud[i].equals("9")){
				jupitatud[i] = "#";
			}
		}
		s = "";
		for(int i = 0; i < jupitatud.length; i++){
			s = s + jupitatud[i];
		}
		
		return s; 

	}

}
