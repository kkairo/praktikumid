package praktikum1;

//Programm küsib kasutajalt: "kull või kiri?". "Viskab" mündi ja teatab, 
//kas kasutaja arvas õigesti või mitte.
//Vihje: ära hakka jändama mingite stringidega, 
//küsi kasutajalt number; kasuta selleks eelnevalt valmis kirjutatud meetodit. 
//Meetodit võib täiendada ühe String-tüüpi parameetriga, et kasutajale trükitaks välja sobilik lause. 

public class KullKiri {

	public static void main(String[] args) {
		final int KULL = 1;
		final int KIRI = 2;

		int playerMoney = 100;
		int playerBet = 0;
		int dealerResult = 0;

		System.out.println("Kull v6i Kiri");

		while (playerMoney > 0) {
			playerBet = getBet(1, 25);
			dealerResult = juhuArv(1, 2);
			if (dealerResult == KIRI) {
				playerMoney += playerBet;
				System.out.println("KIRI. V6itsid. Sinu konto j22k: " + playerMoney);
			}
			if (dealerResult == KULL) {
				playerMoney -= playerBet;
				System.out.println("KULL. Kaotasid. Sinu konto j22k: " + playerMoney);
			}
			System.out.println();
		}
		System.out.println("Raha sai otsa, m2ng l2bi!");
	}

	public static int juhuArv(int min, int max) {
		return min + (int) (Math.random() * ((max - min) + 1)); // annab arvu max ja min - m6lemad incl
		// return Math.random() * (max - min) + min; annab arvu min incl ja max excl
		
	}

	public static int getBet(int _min, int _max) {
		System.out.println("Sisesta panus");
		int bet = KysiArvu(_min, _max);
		return bet;
	}

	private static int KysiArvu(int _min, int _max) {
		int kasutajaPakkumine = _min - 1;
		while (kasutajaPakkumine < _min || kasutajaPakkumine > _max) {
			System.out.println("Sisesta arv vahemikus " + _min + " ja " + _max);
			kasutajaPakkumine = TextIO.getlnInt();
		}
		return kasutajaPakkumine;
	}

}
