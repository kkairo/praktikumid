package praktikum1;

public class NullideArv {

	public static void main(String[] args) {
		int [] m = {5, 80, 0, 0, 6, 8};
		System.out.println(nullideArv(m));
		
	}

	public static int nullideArv(int[] m) {

		int temp = 0;

		for (int i = 0; i < m.length; i++) {
			if (m[i] == 0) {
				temp++;
			}
		}

		return temp; 

	}

}
