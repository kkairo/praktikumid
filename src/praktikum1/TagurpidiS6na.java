package praktikum1;

import java.util.Scanner;

public class TagurpidiS6na {

	public static void main(String[] args) {
		// loon skänneri
        Scanner sc = new Scanner(System.in);

        System.out.println("Sisesta s6na");
        // saan kasutajalt s6na:
        String word = sc.nextLine();

        //prindin s6na v2lja tagurpidi:
        for (int i = word.length() - 1; i >= 0; i--){
            System.out.print(word.charAt(i));
        }
        sc.close();

	}

}
