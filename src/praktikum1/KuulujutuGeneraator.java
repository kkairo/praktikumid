package praktikum1;

public class KuulujutuGeneraator {

	public static void main(String[] args) {
		String[] nimed1 = {"Kati", "Tiina", "Liisu", "Nele"};
        String[] tegus6nad = {"Paneb", "Leiab", "Näeb", "Teab"};
        String[] nimed2 = {"T6nu", "Martin", "Margus"};

        System.out.println(nimed1[ranGen(nimed1.length)] + " " +
                tegus6nad[ranGen(tegus6nad.length)] + " " +
                        nimed2[ranGen(nimed2.length)]);
    }

    private static int ranGen(int length){
        return (int) (Math.random() * length);
	}
    // integer between [-10,10) - maximum 9
    // int r2 = (int) (Math.random()*20)-10;
    // System.out.println("Integer between -10 and 10: r2 = "+r2);
    
    // integer between [3,7]
    // int r1 = (int) (Math.random()*5)+3;
    // System.out.println("Integer between 3 and 8: r1 = "+r1);
}
