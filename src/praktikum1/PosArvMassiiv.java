package praktikum1;

public class PosArvMassiiv {

	public static void main(String[] args) { //On antud positiivne täisarv n. Kirjutada Java meetod, mis leiab täisarvu, mis saadakse n kümnendesituses numbrite järjekorra ümberpööramise teel. 
		System.out.println(inverse(1234)); // 4321
	}

	public static int inverse(int n) {
		
        int tagurpidi = 0;
        int j22k = 0;
        do{
            j22k = n % 10;
            tagurpidi = tagurpidi*10 + j22k;
            n = n/10;
         
        }while(n > 0);
     
        return tagurpidi;
	}

}
