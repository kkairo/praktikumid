package praktikum7;

public class KuulujutuGeneraator {

	public static void main(String[] args) {
		
		String [] naiseNimed = {"Triin", "Pille", "Kadi"};
		String [] meheNimed = {"Kaarel", "Peeter","Paul"};
		String [] tegevused = {"jooksevab", "s6uavab", "magavad"};
		
		int randomNaiseNimed = (int)(naiseNimed.length * Math.random());
		int randomMehenimed = (int)(meheNimed.length * Math.random());
		int randomTegevused = (int)(tegevused.length * Math.random());
		
		System.out.println(naiseNimed[randomNaiseNimed]+ " ja " + meheNimed[randomMehenimed] + 
				" " + tegevused[randomTegevused] + ".");

	}

}
