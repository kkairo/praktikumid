package praktikum7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


//Kirjutada programm, mis laseb kasutajal sisestada senikaua nimesid, kui kasutaja 
//sisestab nime asemel tühja rea. Seejärel trükkida nimed ekraanile 
//tähestikulises järjekorras. Vihje: selle ülesande kasutamiseks kasuta ArrayList klassi. 
//Sorteerimiseks kasuta Collections.sort() meetodit. 

public class NimedeSortimine {
	
	public static void main(String[] args) {
		ArrayList<String> nimed = new ArrayList<String>();
		
		while(true) {
			System.out.print("Kirjuta nimi: ");
			String nimi = lib.TextIO.getlnString();
			if (nimi.isEmpty()) {
				break;
			}
			nimed.add(nimi);
		}
		
		// sort
		Collections.sort(nimed, null);  
		
		System.out.println("Sisestatud nimed on: ");
		for (String nimi : nimed) {
		    System.out.println(nimi);
		}

	}	
	
}

//	public static void main(String[] args){
//        //loon arraylisti
//        ArrayList<String> names = new ArrayList<String>();
//        //loon sk2nneri
//        Scanner userInput = new Scanner(System.in);
//        String _userInput = "empty";
//        //kysin nimesid
//        System.out.println("Sisesta nimesid: ");
//
//        // <stringinimi>.isEmpty() kontrollib, kas string on tyhi
//        while (!_userInput.isEmpty()){
//
//            //stringi puhul pole try/catch kuigi oluline, sest k6ik 
//        	//sisestatud symbolid sobivad stringiks
//            _userInput = userInput.nextLine();
//
//            //kui polnud tyhi string, lisa nimi arraylist-i
//            if (!_userInput.isEmpty()){
//                names.add(_userInput);
//            }
//        }
//
//        //sordin nimed
//        Collections.sort(names);
//
//        //prindin nimed
//        for (int i = 0; i < names.size(); i++){
//            System.out.println(names.get(i));
//        }
//
//        // sulgen sk2nneri
//        userInput.close();
//    }	
//	
//}
