package praktikum14;

public class Katsetused {

	public static void main(String[] args) {
		
		Punkt minuPunkt = new Punkt(); // uus punkti objekt; kutsutakse välja konstruktor
		minuPunkt.x = 0;
		minuPunkt.y = 0;
		
		Punkt veelYksPunkt = new Punkt(100, 0);
		System.out.println(veelYksPunkt); // kutsutakse välja objekti klassi Punkt toString meetod
		System.out.println(minuPunkt);
		
		Joon minuJoon =  new Joon(minuPunkt, veelYksPunkt);
		System.out.println(minuJoon);
		minuJoon.pikkus();
		
		System.out.println("Joone pikkus on: " + minuJoon.pikkus());
		
		Ring minuRing = new Ring(new Punkt(200, 200), 23.);
		System.out.println(minuRing);
		
	}

}
