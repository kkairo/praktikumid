package praktikum14;

public class Joon {

	Punkt punkt1;
	Punkt punkt2;
	
	public Joon(Punkt x, Punkt y) {
		punkt1 = x;
		punkt2 = y;
	}
	
	@Override
	public String toString() {
		
		return "Joon " + punkt1 + " punktini " + punkt2;
	}
	
	public double pikkus() {
		double pikkus;
		double a = punkt2.y - punkt1.y;
		double b = punkt2.x - punkt1.x;
		return pikkus = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
		
	}
}
