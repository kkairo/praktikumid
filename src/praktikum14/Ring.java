package praktikum14;

public class Ring {

	Punkt keskpunkt;
	double raadius;
	
	public Ring(Punkt punkt, double r) {
		keskpunkt = punkt;
		raadius = r;
		// TODO Auto-generated constructor stub
	}

	public double ymberm66t() {
		return 2 * Math.PI * raadius;
		
	}
	
	public double pindala() {
		return Math.PI * Math.pow(raadius, 2);
	}
	
	@Override
	public String toString() {
		
		return "Ring(" + keskpunkt + ", " + raadius + ") ja ümbermõõt on: " + ymberm66t() + " ning pindala on:" + pindala();
	}
}
