package praktikum14;

public class Punkt { // extends Object - seal on konstruktor-meetod
	
	int x;
	int y;
	
	public Punkt(int x, int y) {
		this.x = x; // ülemine x saab siin sulgudes oleva väärtuse
		this.y = y;// muutuja nimed v6ivad konliktida, kasutame this.
		
	}
	public Punkt() {
		// Luuakse punkt objekt millel pole koordinaate määratud
	
	}

	@Override //kirjutab ülemklassi meetodi üle
	public String toString() {	
		return "Punkt(x=" + x + ", y=" + y + ")";		
		}
}
