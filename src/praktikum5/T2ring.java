package praktikum5;

import praktikum1.KullKiri;

//Programm viskab kaks täringut mängijale ja kaks 
//täringut endale (arvutile), arvutab mõlema mängija silmade summad ja teatab, kellel oli rohkem.

public class T2ring {

	public static void main(String[] args){
        int kasutajaSumma = KullKiri.juhuArv(1,6) + KullKiri.juhuArv(1,6);
        int arvutiSumma = KullKiri.juhuArv(1,6) + KullKiri.juhuArv(1,6);

        if (kasutajaSumma > arvutiSumma){
            System.out.printf("Arvuti sai %d ja sina %d. Sina v6itsid!", arvutiSumma, kasutajaSumma);
        }
        else if (kasutajaSumma < arvutiSumma){
            System.out.printf("Arvuti sai %d ja sina %d. Sina kaotasid!", arvutiSumma, kasutajaSumma);
        }
        else{
            System.out.printf("Arvuti sai %d ja sina %d. J2ite viiki!", arvutiSumma, kasutajaSumma);
        }
    }
}

//%s - Take the next argument and print it as a string
//%d - Take the next argument and print it as an int/decimal
//%c refers to character
