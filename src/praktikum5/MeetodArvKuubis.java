package praktikum5;

import lib.TextIO;

public class MeetodArvKuubis {

	public static void main(String[] param) {

		System.out.println("Palun sisesta mingi arv: ");
		int arv = TextIO.getlnInt();
		int arvKuubis = kuup(arv);
		System.out.println(arvKuubis);
		System.out.println(kuup(7));

	}

	public static int kuup(int sisendV22rtus) {
		int tagastusV22rtus = (int) Math.pow(sisendV22rtus, 3);
		return tagastusV22rtus;

	}

	public static int kasutajaSisestus(int min, int max) {

		int sisendArv; // do-while ts�kli korral peab miskip�rast muutujad enne
						// ts�klit �ra m��rama
		do {
			System.out.print("Sisesta arv: ");
			sisendArv = TextIO.getlnInt();
		} while (sisendArv < min || sisendArv > max);
		System.out.println(sisendArv);
		return sisendArv;

	}

	public static int suvalineT2isarvVahemikus(String kysimus, int min, int max) { //kui string ei taha kasutada sisesta ""

		int suvaline = min + (int) (Math.random() * ((max - min) + 1));
		System.out.print(kysimus);
		return suvaline;

	}
}

// public static int ArvuKuup(int _arv)
// {
// return (int) Math.pow((double)_arv, 3);
// }
