package praktikum5;

import praktikum1.KullKiri;

//Kirjutada programm, mis viskab 5 täringut ning arvutab silmade summa
//NB! Lahendus peab olema tehtud tsükliga.
//Vihje: tsüklis summa arvutamiseks tuleb summat sisaldav muutuja enne 
//tsükli käivitamist väärtustada nulliga ja tsükli kehas liita 
//sellele üks haaval väärtusi otsa (sum = sum + ...)

public class ViisT2ringut {

	public static void main(String[] args){
        int silmadeSumma = 0;
        for (int i = 0; i < 5; i++) {
            silmadeSumma += KullKiri.juhuArv(1,6);
        }
        System.out.println("Silmade summa: " + silmadeSumma);
    }	
	
}
