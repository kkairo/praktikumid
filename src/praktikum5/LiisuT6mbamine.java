package praktikum5;

import lib.TextIO;

//Küsida kasutajalt inimeste arv. Väljastada random number 
//vahemikus 1 kuni arv (kaasaarvatud)
//NB! Kontrollida, et töötab õigesti: st. öeldes mitu korda 
//järjest arvuks 3, peab võimalike vastuste hulgas olema nii ühtesid, kahtesid kui kolmi.

public class LiisuT6mbamine {
	public static void main(String[] args) {
		
		// Liisutőmbamises osalevate inimeste arvu küsimine
		System.out.print("Sisesta inimeste arv: ");
		int inimesteArv = TextIO.getlnInt();
		
		// Meetodi "suvlineT2isarvVahemikus" kasutamine
		int v2ljaValitu = MeetodArvKuubis.suvalineT2isarvVahemikus("", 1, inimesteArv);
		
		System.out.print("Väljavalitud on " + v2ljaValitu + ". inimene.");

	}
	
//	public static void main(String[] args){
//        int inimesteArv = 0;
//        int juhuArv = 0;
//        Scanner stdin = new Scanner(System.in);
//
//        System.out.print("Sisesta inimeste arv: ");
//
//        try {
//            inimesteArv = stdin.nextInt();
//        }
//        catch (InputMismatchException e){
//            System.out.println(e);
//        }
//        // saan juhuarvu kasutades klassis "KullKiri" asuvat meetodit "juhuArv"
//        juhuArv = KullKiri.juhuArv(1, inimesteArv);
//
//        System.out.println("Valitud sai " + juhuArv + ". inimene");
//        stdin.close();
//    }

}
