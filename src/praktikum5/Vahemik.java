package praktikum5;

import lib.TextIO;

//Kirjutada meetod, mis saab parameetritena kaks arvu (vahemiku) ning tagastab 
//kasutaja sisestatud arvu, juhul kui see on lubatud vahemikus. 
//Senikaua, kui kasutaja sisestab midagi ebasobivat, 
//käseb meetod kasutajal arvu uuesti sisestada.

public class Vahemik {
	
	public static void main(String[] param) {
		
		int kasutajaSisestas = kasutajaSisestus(1, 10);
		System.out.println("kasutajaSisestus meetod tagastas: " + kasutajaSisestas);
	}

    public static int kasutajaSisestus(int min, int max) {
        while (true) {
            System.out.println("Palun sisesta number vahemikus " + min + " kuni " + max);
            int sisestus = TextIO.getlnInt();
            if (sisestus >= min && sisestus <= max) {
                return sisestus;
            } else {
                System.out.println("Vigane sisestus! Proovi uuesti.");
            }
        }
    }
    
}
