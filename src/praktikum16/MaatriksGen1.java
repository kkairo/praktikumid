package praktikum16;

public class MaatriksGen1 {

	//Koostage Java meetod, mis genereerib parameetrina etteantud n järgi 
//	niisuguse n korda n täisarvumaatriksi,
	//mille iga elemendi väärtuseks on selle elemendi reaindeksi 
//	ja veeruindeksi summa ruut (indeksid algavad nullist).
	//
	//   public static int[][] muster (int n)

	    public static void main(String[] args) {
	        int[][] matrix = muster(3);
	        for (int i = 0; i < matrix.length; i++) {
	            for (int j = 0; j < matrix.length; j++) {
	                System.out.printf("%3d ", matrix[i][j]);
	            }
	            System.out.println();
	        }
	    }
	    public static int[][] muster(int n) {
	        int[][] matrix = new int[n][n];
	        for (int i = 0; i < n; i++) {
	            for (int j = 0; j < n; j++) {
	                matrix[i][j] = ((i + j) * (i + j));
	            }
	        }
	        return matrix;
	    }
	}	
	

