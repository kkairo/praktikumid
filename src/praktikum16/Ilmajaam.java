package praktikum16;

public class Ilmajaam {

	//3 aasta keskmised kuu õhutemperatuurid on antud 50x12 maatriksina. Leida:
//  millal (mis aastal)  oli kõige soojem (või külmem)  jaanuar (või mistahes teine kuu)
//  millal oli kõige soojem (või külmem)  aasta keskmine
//  milline kuu on kõige külmem (või soojem)  50 aasta keskmisena
//  millal (aasta ja kuu) esines kõige madalam (kõrgem) kuu keskmine.

public static void main(String[] args) {
  double[][] ilmad = new double[][]{
          {-5.4, -7.2, 4.6, 6.7, 8.1, 15.5, 17.7, 16.3, 16.7, 10.6, 4.6, -3.9},
          {-7.6, -10.2, 2.6, 4.6, 10.5, 16.6, 20.1, 15.6, 15.7, 9.9, 4.3, -5.9},
          {-10.4, -7.5, -1.3, 4.5, 9.1, 13.5, 15.6, 16.4, 13.7, 11.6, 0.6, -3.5},
          {-90.5, -7.4, -0.5, 5.1, 10.2, 12.6, 21.3, 200.6, 19.7, 12.5, 3.1, -10.5}
  };

  boolean soe = true;
  boolean kylm = false;
//  System.out.println(misAastalKuuMaxMin(soe, 0, ilmad));

//  System.out.println(aastaKeskmine(kylm, ilmad));

//  System.out.println(k6igeSoojemKylmemKuu(soe, ilmad));

  int[] a = misAastaKuuKesk(kylm, ilmad);
  for (int i = 0; i < 2; i++) {
      System.out.println(a[i]);
  }

}

public static int misAastalKuuMaxMin(Boolean soeKylm, int kuuNr, double[][] ilmad) {

  double max = ilmad[0][kuuNr];
  double min = ilmad[0][kuuNr];
  int aastaMax = 0;
  int aastaMin = 0;
  for (int i = 0; i < ilmad.length; i++) {
      if (max < ilmad[i][kuuNr]) {
          max = ilmad[i][kuuNr];
          aastaMax = i;
      }
      if (min > ilmad[i][kuuNr]) {
          min = ilmad[i][kuuNr];
          aastaMin = i;
      }
  }
  if (soeKylm == true) {
      return aastaMax;
  } else {
      return aastaMin;
  }
}

public static int aastaKeskmine(boolean soeKylm, double[][] ilmad) {

  double[] keskmised = new double[ilmad.length];
  for (int i = 0; i < ilmad.length; i++) {
      double tempSumma = 0;
      for (int j = 0; j < ilmad[i].length; j++) {
          tempSumma = tempSumma + ilmad[i][j];
      }
      keskmised[i] = tempSumma / 12;
  }
  for (int i = 0; i < keskmised.length; i++) {
      System.out.println(keskmised[i]);
  }
  double max = keskmised[0];
  double min = keskmised[0];
  int aastaMax = 0;
  int aastaMin = 0;
  for (int i = 0; i < keskmised.length; i++) {
      if (max < keskmised[i]) {
          max = keskmised[i];
          aastaMax = i;
      }
      if (min > keskmised[i]) {
          min = keskmised[i];
          aastaMin = i;
      }
  }
  if (soeKylm == true) {
      return aastaMax;
  } else {
      return aastaMin;
  }
}

public static int k6igeSoojemKylmemKuu(boolean soeKylm, double[][] ilmad) {

  double[] summad = new double[12];
  double[] keskmised = new double[12];
  for (int i = 0; i < ilmad.length; i++) {
      double tempSumma = 0;
      for (int j = 0; j < ilmad[i].length; j++) {
          summad[j] = summad[j] + ilmad[i][j];
      }
  }
  for (int i = 0; i < 12; i++) {
      keskmised[i] = summad[i] / ilmad.length;
  }
  for (int i = 0; i < keskmised.length; i++) {
      System.out.println(keskmised[i]);
  }
  double max = keskmised[0];
  double min = keskmised[0];
  int kuuMax = 0;
  int kuuMin = 0;
  for (int i = 0; i < keskmised.length; i++) {
      if (max < keskmised[i]) {
          max = keskmised[i];
          kuuMax = i;
      }
      if (min > keskmised[i]) {
          min = keskmised[i];
          kuuMin = i;
      }
  }
  if (soeKylm == true) {
      return kuuMax;
  } else {
      return kuuMin;
  }
}

public static int[] misAastaKuuKesk(boolean soekylm, double[][] ilmad) {

  double maxKesk = ilmad[0][0];
  double minKesk = ilmad[0][0];
  int aastaMax = 0;
  int kuuMax = 0;
  int aastaMin = 0;
  int kuuMin = 0;
  for (int i = 0; i < ilmad.length; i++) {
      for (int j = 0; j < ilmad[i].length; j++) {
          if (maxKesk < ilmad[i][j]) {
              maxKesk = ilmad[i][j];
              aastaMax = i;
              kuuMax = j;
          }
          if (minKesk > ilmad[i][j]) {
              minKesk = ilmad[i][j];
              aastaMin = i;
              kuuMin = j;
          }
      }
  }
  if (soekylm == true) {
      int[] kuuJaAastaMax = new int[]{aastaMax, kuuMax};
      return kuuJaAastaMax;
  } else {
      int[] kuuJaAastaMin = new int[]{aastaMin, kuuMin};
      return kuuJaAastaMin;
  }
}	
	
}
