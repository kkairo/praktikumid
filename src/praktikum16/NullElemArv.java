package praktikum16;

public class NullElemArv {
	
	//Koostage Java-meetod, mis leiab etteantud täisarvude massiivi m nulliga võrduvate elementide arvu.

	    public static void main(String[] args) {
	        int[] array = new int[]{0, 2, 0, 2, 33, 424, 00, 4124, 0};
	        System.out.println(nullideArv(array));
	    }

	    public static int nullideArv(int[] m) {
	        int nulls = 0;
	        for (int i = 0; i < m.length; i++) {
	            if (m[i] == 0) {
	                nulls = nulls + 1;
	            }
	        }
	        return nulls;
	    }
	}

