package praktikum16;

import java.applet.Applet;
import java.awt.*;

// Koostada  rakend, mis joonistab n vagunist koosneva rongi
// (n kuulub lõiku 1 kuni 8). Ühe vaguni joonistamine vormistada eraldi meetodina.
public class Rong extends Applet {
    private Graphics g;

    public void paint(Graphics g) {
//        g.setColor(Color.black);
//        g.fillRect(20,20, 10, 4);
        this.g = g;
        int n = 5;
        for (int i = 20; i <= 120 * n; i = i + 120) {

            vedur(i, 100);
        }

    }

    public void vedur(int x, int y) {
        g.setColor(Color.black);
        g.fillRect(x, y, 10, 4);
        g.setColor(Color.blue);
        g.fillRect(x + 10, y - 42, 110, 50);


    }
}