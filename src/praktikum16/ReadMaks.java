package praktikum16;

//Koostage Java meetod etteantud täisarvumaatriksi m reamaksimumide massiivi leidmiseks(massiivi i-s element on
//maatriksi i-nda rea suurima elemendi väärtus).Read võivad olla erineva pikkusega.

public class ReadMaks {
 public static void main(String[] args) {
     int[][] matrix = new int[][]{{110, 2, 3}, {40, 5, 600, 1}, {20, 80}};
     for (int i = 0; i < matrix.length; i++) {
         System.out.print(reaMaxid(matrix)[i] + " ");
     }
 }

 public static int[] reaMaxid(int[][] m) {
     int[] reaMax = new int[m.length];
     for (int i = 0; i < m.length; i++) {
         int maxValue = m[i][0];
         for (int j = 0; j < m[i].length; j++) {
             if (m[i][j] > maxValue) {
                 maxValue = m[i][j];
             }
         }
         reaMax[i] = maxValue;
     }
     return reaMax;
 }
}
