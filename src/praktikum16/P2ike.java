package praktikum16;

import java.applet.Applet;
import java.awt.*;
import java.util.GregorianCalendar;

// Koostada rakend (applet), mis joonistab n kiirega kollase päikese (n kuulub lõiku 3 kuni 60).
public class P2ike extends Applet {

    public void paint(Graphics g) {
        int w = getWidth();
        int h = getHeight();
        // Keskpunkt
        int x0 = w / 2;
        int y0 = h / 2;
        int r = 100; // Raadius
        int x, y;
        double t;

        g.setColor(Color.blue);
        g.fillRect(0, 0, w, h);
        g.setColor(Color.yellow);
        g.fillOval(x0 - r / 2, y0 - r / 2, r, r);

        int n = 60;
        for (t = -Math.PI; t < Math.PI; t = t + Math.PI / n ) {
            x = (int) (r * Math.cos(t) + x0);
            y = (int) (r * Math.sin(t) + y0);
            g.drawLine(x0, y0, x, y);
        }
    }
}