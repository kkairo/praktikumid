package praktikum16;

public class MassiivKeskm {
	
	//Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d 
//	pőhjal niisuguste elementide arvu, mis on rangelt
	// suuremad kőigi elementide aritmeetilisest keskmisest 
//	(aritmeetiline keskmine = summa / elementide_arv).

	    public static void main(String[] args) {
	        double[] m = new double[]{12.3, 13.3, 100.2, 0, 32.12, 0.1};
	        System.out.println(suuremKeskmisest(m));
	    }

	    public static int suuremKeskmisest(double[] massiiv) {
	        double summa = 0;
	        int suurem = 0;
	        for (int i = 0; i < massiiv.length; i++) {
	            summa = summa + massiiv[i];
	        }
	        for (int j = 0; j < massiiv.length; j++) {
	            if ((summa / massiiv.length) < massiiv[j]) {
	                suurem = suurem + 1;
	            }
	        }
	        return suurem;
	    }

}
