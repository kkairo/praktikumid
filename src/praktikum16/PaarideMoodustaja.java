package praktikum16;

//On antud kaks nimede massiivi - poisid ja tüdrukud. Koostada programm,
//mis seob iga poisiga erineva juhuslikult valitud tüdruku. Väljastada paaride nimekiri.
public class PaarideMoodustaja {
 public static void main(String[] args) {
     String[] boys = new String[]{"Mikk", "Martin", "Endel", "Hermo"};
     String[] girls = new String[]{"Mari", "Eve", "Piret", "Evelyn"};

     // Suvalite numbrite massiiv kordumatute elementidega
     int[] randomIndecies = new int[girls.length];
     for (int i = 0; i < randomIndecies.length; i++) {
         randomIndecies[i] = (int) (Math.random() * (randomIndecies.length));
         for (int j = 0; j < i; j++) {
             if(randomIndecies[i] == randomIndecies[j]){
                 i--;
                 break;
             }
         }
     }
     for( int i = 0; i < boys.length; i++){
         System.out.println( boys[randomIndecies[i]] + " " + girls[randomIndecies[i]]);
     }
 }
}
