package praktikum16;

//Sportlase punktisumma arvutatakse üksikkatsetest saadud punktide summana, millest on maha võetud kahe
//halvima katse tulemused (üksikkatseid on rohkem kui kaks). Kirjutada Java meetod, mis arvutab punktisumma
//üksikkatsete tulemuste massiivi põhjal. Parameetriks olevat massiivi muuta ei tohi.
//
// public static int score (int[] points)
public class SkoorMassiiv {

  public static void main(String[] args) {

      int[] scoreMatrix = new int[]{10, 1, 10, 20, 2, 15};
      System.out.println(score(scoreMatrix));

  }

  public static int score(int[] points) {
      int score = 0;
      int min1 = points[0];
      int min1Index = 0;
      int min2 = points[1]; // min2 v6ib olla alguses suvaline arv

      if (points.length < 3) {
          return 0;
      } else {
          for (int i = 0; i < points.length; i++) {
              if (points[i] < min1) {
                  min1 = points[i];
                  min1Index = i;
                  min2 = points[0];
              }
          }
          for (int i = 0; i < points.length; i++) {
              if (points[i] < min2 && i != min1Index) {
                  min2 = points[i];
              }
          }
          for (int element : points) {
              if (element != min1 && element != min2) {
                  score += element;
              }
          }
//          System.out.println(min1);
//          System.out.println(min2);
          return score;
      }
  }
}