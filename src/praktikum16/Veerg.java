package praktikum16;

//Koostage Java meetod etteantud täisarvumaatriksi m veerusummade massiivi leidmiseks
//(massiivi j-s element on maatriksi j-nda veeru summa). Arvestage, et m read võivad olla erineva pikkusega.
//
// public static int[] veeruSummad (int[][] m)
public class Veerg {
  public static void main(String[] args) {
      int[][] matrix = new int[][]{{1, 2, 3}, {4, 5, 6, 1}, {0, 8}};
      for(int i = 0; i< veeruSummad(matrix).length; i++){
          System.out.println(veeruSummad(matrix)[i]);
      }
  }

  public static int[] veeruSummad(int[][] m) {
      int pikimRida = m[0].length;
      for (int r = 0; r < m.length; r++) {
          if (m[r].length > pikimRida) {
              pikimRida = m[r].length;
          }
      }
      int[] summad = new int[pikimRida];
      for (int i = 0; i < m.length; i++) {
          for (int j = 0; j < m[i].length; j++) {
              summad[j] = summad[j] + m[i][j];
          }
      }
      return summad;
  }
}
