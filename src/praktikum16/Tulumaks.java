package praktikum16;

//Koostada programm, mis arvutab tulumaksu suuruse aastas,
//kui on antud palk, maksuvaba miinimum ja tulumaksu määr.
public class Tulumaks {
 public static void main(String[] args) {
     double palk = 10000;
     double maksuvaba = 170;
     double tulumaksuM22r = 0.2;
     double tuluMaksAastas = ((palk - maksuvaba) * 0.2) * 12;
     System.out.println(tuluMaksAastas);
 }
}
