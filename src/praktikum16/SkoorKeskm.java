package praktikum16;

//Sportlase esinemist hindab n>2kohtunikku.Hinnete hulgast eemaldatakse kõige madalam ja
//kõige kõrgem ning leitakse ülejäänud n-2hinde aritmeetiline keskmine.Kirjutada Java-meetod hinde arvutamiseks.

public class SkoorKeskm {
  public static void main(String[] args) {
      int[] scoreMatrix = new int[]{5, 1, 1, 1, 10, 20, 15, 21, 21, 21, 21};
      System.out.println(score(scoreMatrix));
  }

  public static double score(int[] points) {
      double score;
      double summa = 0;
      int min = points[0];
      int max = points[0];

      if (points.length < 3) {
          return 0;
      } else {
          for (int i = 0; i < points.length; i++) {
              if (points[i] < min) {
                  min = points[i];
              }
          }
          for (int j = 0; j < points.length; j++) {
              if (points[j] > max) {
                  max = points[j];
              }
          }
          for (int i = 0; i < points.length; i++) {
              summa += points[i];
          }
      }
      score = (summa - min - max) / (points.length - 2);
      return score;
  }
}
