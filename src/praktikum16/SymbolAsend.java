package praktikum16;

//Koostada Java meetod, mis asendab parameetrina etteantud sõnes s kõik numbrid märgiga '#'.
public class SymbolAsend {
 public static void main(String[] args) {
     System.out.println(asenda("023 ababa123babaa nan123 0932815087409478dfhksj0239856231"));
 }

 public static String asenda(String s) {
     String[] charArray = s.split("");
     for (int i = 0; i < s.length(); i++) {
         if (charArray[i].equals("0")) {
             charArray[i] = "#";
         }
         if (charArray[i].equals("1")) {
             charArray[i] = "#";
         }
         if (charArray[i].equals("2")) {
             charArray[i] = "#";
         }
         if (charArray[i].equals("3")) {
             charArray[i] = "#";
         }
         if (charArray[i].equals("4")) {
             charArray[i] = "#";
         }
         if (charArray[i].equals("5")) {
             charArray[i] = "#";
         }
         if (charArray[i].equals("6")) {
             charArray[i] = "#";
         }
         if (charArray[i].equals("7")) {
             charArray[i] = "#";
         }
         if (charArray[i].equals("8")) {
             charArray[i] = "#";
         }
         if (charArray[i].equals("9")) {
             charArray[i] = "#";
         }
     }
     String replaced = "";
     for (int j = 0; j < s.length(); j++) {
         replaced = replaced + charArray[j];
     }
     return replaced;
 }
}
