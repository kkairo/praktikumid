package praktikum16;

//On antud positiivne täisarv n.Kirjutada Java meetod,mis leiab täisarvu,
//mis saadakse n kümnendesituses numbrite järjekorra ümberpööramise teel.

public class TagurpidiArv {
 public static void main(String[] args) {
     System.out.println(inverse(1234));
     System.out.println(inverseVar2(5678));
 }

 public static int inverse(int n) {
     String arv = "";
     for (int i = Integer.toString(n).length() - 1; i >= 0; i--) {
         arv = arv + Integer.toString(n).charAt(i);
     }
     int tagurpidi = Integer.parseInt(arv);
     return tagurpidi;
 }

 public static int inverseVar2(int n) {
     int tagurpidi = 0;
     int j22k;
     do {
         j22k = n % 10;
         tagurpidi = tagurpidi * 10 + j22k;
         n = n / 10;
     } while (n > 0);
     return tagurpidi;
 }
}