package praktikum16;

public class LeiaMaatriksist {

	//Leida etteantud reaalarvude maatriksis niisuguse rea indeks,
	//milles on kõige vähem negatiivseid elemente.
	    public static void main(String[] args) {
	        int[][] matrix = new int[][]{{110, -2, 3}, {-40, 5, 600, -1}, {20, 80}, {1, -1}};

	        System.out.println(leiaMaatriksist(matrix));
	    }

	    public static int leiaMaatriksist(int[][] mat) {
	        int[] neg = new int[mat.length];// loome massiivi neg el jaoks; pikkus sama kuipalju ridu on 3
	        for (int i = 0; i < mat.length; i++) {
	            int reasNeg = 0;
	            for (int j = 0; j < mat[i].length; j++) {
	                if (mat[i][j] < 0) { //kas on väiksem nullist
	                    reasNeg = reasNeg + 1;
	                }
	            }
	            neg[i] = reasNeg;// uus massiiv kuhu pannakse iga rea neg elementide arvu {
	        }

//	        for (int i = 0; i < neg.length; i++) {
//	            System.out.print(neg[i] + " ");
//	        }

	        int max = neg[0];// neg kohal 0
	        for (int i = 0; i < neg.length; i++) {
	            if (max > neg[i]) {//leiame väikseima arvu massiivist
	                max = i;
	            }
	        }
	        return max;
	    }	
	
}
