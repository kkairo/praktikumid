package praktikum16;

import java.applet.Applet;
import java.awt.*;
import java.util.AbstractCollection;

// Koostada rakend, mis joonistab koordnaatteljed ning
// funktsiooni y=sinx + cosx graafiku lõigul -5 kuni 5.
public class Siinus extends Applet {
    public void paint(Graphics g) {
        int w = getWidth();
        int h = getHeight();
        int x0 = w / 2;
        int y0 = h / 2;
        g.setColor(Color.BLACK);
        g.drawLine(x0 - 200, y0, x0 + 200, y0);
        g.drawLine(x0, y0 - 200, x0, y0 + 200);

    }
}